# sign-translator

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

React assignment for Noroff. A react app build up using VSCode and several third party packages (like axios and bootstrap 5). The application is made up of three elements: A login page, a translator page, and a profile. The Translator and profile are protected with authentication. On the translator page the user can type in a word or sentence is then translated into sign language letter by letter, this word or sentence is stored on an api hosted by heroku. On the profile the user can see the last 10 translations and clear the entire history on the API.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

- Fork or clone locally
- run NPM install on the root directory
- run NPM start

## Usage

- Fill in a username on the login page, no password is needed
- Fill in a word or sentence to translate after the redirection. This word cannot contain special characters or letters, and can only be 40 characters long. The sign language images will appear below your input.
- The navbar holds two links: A profile and logout
- Profile will show your last 10 translations and a button to clear the entire history
- The logout button logs you out of the application and redirects back to the login page.

## Maintainers

[@rikdortmans](https://github.com/rikdortmans)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 rik dortmans
