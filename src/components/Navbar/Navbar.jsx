import { Container, Image, Nav, NavbarBrand } from "react-bootstrap";
// import Navbar from 'react-bootstrap/Navbar'
import { NavLink, useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { storageDelete } from "../../utils/storage";
const Navbar = () => {
    const { user, setUser } = useUser();
    const navigate = useNavigate();

    /**
     * Function to handle the click event of the Logout button
     * Confirms with the user if the click was intentional
     * If it was, will proceed with removing the user from local storage and the state
     * Will then redirect to the main page
     */
    const handleLogoutClick = () => {
        if (window.confirm("Are you sure you want to logout?")) {
            console.log("LOGOUT");
            storageDelete(user);
            setUser(null);
            navigate("/");
        }
        else {
            navigate("/translator");
        }
    }
    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-dark">
                <div className="container-fluid">
                    <a className="navbar-brand"><Image alt="Logo" src="img/logo.png" id="navbar-logo"/> Sign Translator</a>
                    {user !== null &&
                        <div className="d-flex" id="">
                            <NavLink className="nav-link" to="/translator">Translator</NavLink>
                            <NavLink className="nav-link" to="/profile">Profile</NavLink>
                            <a className="nav-link" onClick={handleLogoutClick}>Logout</a>
                        </div>
                    }
                </div>
            </nav>
        </>
    )
}

export default Navbar