import { Image } from "react-bootstrap"

const TranslationImage = ({char, key}) =>{
    return(
        <Image className="translated-char" src={`img/individual_signs/${char}.png`}key={`${key}`} />
    )
}

export default TranslationImage