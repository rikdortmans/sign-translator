import { useState } from "react";
import { Button } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { createTranslation, getAllTranslations } from "../../api/translation";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageSave } from "../../utils/storage";
import TranslationImage from "./TranslationImage";

const toTranslateConfig = {
    required: true,
    maxLength: 40,
}
const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
const numbers = /[0-9]/;

const TranslatorForm = () => {
    const { user } = useUser();
    const { register, handleSubmit, formState: { errors }, } = useForm();
    const [translation, setTranslation] = useState(null);
    const [apiError, setApiError] = useState(null);

    /**
     * Function to handle the submission of the translator form
     * First tests the string against special characters and numbers using regex
     * Splits the string into separate characters, loops through them, and converts them into TranslationImages
     * After the state has been set, the function calls the translation API to update the translations
     * Followed by appropriate error handling
     * @param {string} toTranslate - the text that needs to converted to sign-language images
     * Sets the state of translation
     */
    const onSubmit = async ({ toTranslate }) => {
        if (specialChars.test(toTranslate) || numbers.test(toTranslate)) {
            setTranslation("Cannot translate special characters");
        }
        else {
            const separatedChars = toTranslate.split("");
            const translatedChars = [];
            for (let i = 0; i < separatedChars.length; i++) {
                const char = separatedChars[i];
                if (char !== " ") {
                    //<img src={char}.png>
                    translatedChars.push(<TranslationImage char={char} key={i}/>)
                }
                else {
                    translatedChars.push(char);
                }
            }
            setTranslation(translatedChars);

            const [getError, getResponse] = await getAllTranslations(user);
            if (getResponse !== null) {
                user.translations = getResponse;

                const [error, response] = await createTranslation(user, toTranslate);
                if (response !== null) {
                    user.translations = [...user.translations, toTranslate];
                    storageSave(STORAGE_KEY_USER, user);
                }
                if (error !== null) {
                    setApiError(error);
                }
            }
            else if (getError !== null) {
                setApiError(getError);
            }
        }
    }

    /**
     * Error handler for the API
     * @returns {<span>} Contains the error
     */
    const errorMessage = () => {
        if (!errors.toTranslate) {
            return null
        }

        if (errors.toTranslate.type === 'required') {
            return <span>Translation is required</span>
        }

        if (errors.toTranslate.type === 'maxLength') {
            return <span>Translation to long, can only translate 40 characters at a time</span>
        }
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div id="form-container" className="mb-3" >
                    <label id="form-label" className="form-label" htmlFor="toTranslate">Translate your text</label>
                    <input className="form-control" type="text" placeholder="Translate your text" {...register("toTranslate", toTranslateConfig)} />
                </div>
                <Button variant="primary" type="submit">Translate text</Button>
            </form>

            {errorMessage()}
            {apiError && <p>{apiError}</p>}
            <section>
                {translation &&
                    <picture>{translation}</picture>}
            </section>
        </>
    )
}

export default TranslatorForm;