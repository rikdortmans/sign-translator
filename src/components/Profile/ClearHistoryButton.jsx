import { Button } from "react-bootstrap"

const ClearHistoryButton = ({onSelect}) =>{
    return(
        <>
        <Button variant="danger" onClick={()=>onSelect()}>Clear History</Button>
        </>
        
    )
}

export default ClearHistoryButton;