import { nanoid } from "nanoid";
import { useEffect, useState } from "react";
import { deleteAllTranslations, getAllTranslations } from "../../api/translation";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageRead, storageSave } from "../../utils/storage";
import TranslationImage from "../Translator/TranslationImage";
import ClearHistoryButton from "./ClearHistoryButton";

const TranslationHistory = () => {

    const { user } = useUser();
    const [translations, setTranslations] = useState(null);

    /**
     * Fills up the last 10 translations of the user, the latest shown first, the moment the page loads
     */
    useEffect(() => {
        if (translations === null) {
            setTranslations(translationArray.reverse().slice(0, 10).map(translation => {
                return <p key={nanoid()}>{translation}</p>
            }))
        }
    })

    const translationArray = user.translations;

    /**
     * Function to handle the click event of the Clear History Button
     * First confirms with the user
     * Will then proceed to delete all translations of the user from the API
     * If that was successful it will then get the empty translation array and set it to the user state
     * The updated user is then updated in the local storage. 
     */
    const handleClearClicked = async () => {
        if (window.confirm("Are you sure you want to delete your translation history?")) {
            const [error, response] = await deleteAllTranslations(user);

            if (error !== null) {
                console.log(error);
            }
            if (response !== null) {
                const [error, translationResponse] = await getAllTranslations(user);
                if (error !== null) {
                    console.log(error);
                }
                if (translationResponse != null) {

                    user.translations = translationResponse;
                    setTranslations(user.translations);
                    storageSave(STORAGE_KEY_USER, user);
                }


            }

        }
    }

    return (
        <>
            <h2>translation history</h2>
            {translations}
            <ClearHistoryButton onSelect={handleClearClicked} />
        </>
    )
}

export default TranslationHistory