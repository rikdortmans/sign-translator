import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import { loginUser } from '../../api/user';
import { STORAGE_KEY_USER } from '../../const/storageKeys';
import { useUser } from '../../context/UserContext';
import { storageSave } from '../../utils/storage';

const usernameConfig = {
    required: true,
    minLength: 2,
}

const LoginForm = () => {
    const { register, handleSubmit, formState: { errors }, } = useForm();
    const { user, setUser } = useUser();
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null);

    /**
     * If user is logged in, redirect to /translator
     */
    useEffect(() => {
        if (user !== null) {
            navigate('translator')
        }
    }, [user, navigate]);

    /**
     * Function to handle the submission of the login form
     * Calls the user api to log the user in (or to create a new one if it doesn't exist)
     * Stores the user locally and sets the User State which will trigger a redirect from the UseEffect()
     * @param {string} username of the user
     */
    const onSubmit = async ({ username }) => {
        setLoading(true);
        const [error, response] = await loginUser(username);
        console.log(response);

        if (error !== null) {
            setApiError(error);
        }
        if (response !== null) {
            storageSave(STORAGE_KEY_USER, response);
            setUser(response);
        }

        setLoading(false);
    };

    /**
     * Handles which error message needs to be shown
     * Triggered by the UserConfig
     * @returns {<span>}
     */
    const errorMessage = () => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        if (errors.username.type === 'minLength') {
            return <span>Username is too short</span>
        }
    }

    return (
        <>
            <form  onSubmit={handleSubmit(onSubmit)}>
                <div id="form-container" className="mb-3">
                    <label id='form-label' htmlFor="username" className="form-label">Username: </label>
                    <input type="text" className="form-control" {...register("username", usernameConfig)}/>
                    {errorMessage()}

                </div>
                {!loading && <Button variant='primary' type="submit" >Log in</Button>}
                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}

            </form>

        </>
    )
}

export default LoginForm