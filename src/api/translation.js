import axios from "axios"

const API_URL = process.env.REACT_APP_API_URL
const API_KEY = process.env.REACT_APP_API_KEY

const headers = {
    'Content-Type': 'application/json',
    'x-api-key': API_KEY,
}


/**
 * 
 * @param {user} user 
 * @param {string} translation 
 * @returns Array with [Error, Response.data], one will be null
 * 
 * Updates the user with a new valid translation using axios.patch
 * Used in /components/Translator/TranslatorForm.jsx
 */
export const createTranslation = async (user, translation) => {
    const content = {
        username: user.username,
        translations: [...user.translations, translation],
    }

    try {
        const response = await axios.patch(`${API_URL}/${user.id}`, content, { headers })

        if (response.status !== 200) {
            throw new Error("Could not update translations");

        }

        return [null, response.data];
    } catch (error) {
        return [error.message, null];
    }
}

/**
 * 
 * @param {user} user 
 * @returns Array with [Error, Response.data], one will be null
 * 
 * This functions retrieves all translations of the user stored on the API
 * Used in /components/Translator/TranslatorForm.jsx
 * Used in /components/Profile/TranslatorHistory.jsx
 */
export const getAllTranslations = async (user) => {
console.log("Getting translastions");
    try {
        const response = await axios.get(`${API_URL}/${user.id}`);
        if (response.status !== 200) {
            throw new Error("Could not update translations");

        }

        return [null, response.data.translations];
    } catch (error) {
        return [error.message, null];
    }
}

/**
 * 
 * @param {user} user 
 * @returns Array with [Error, Response.data], one will be null
 * 
 * This functions deletes all translations of the user stored on the API
 * Used in /components/Profile/TranslatorHistory.jsx
 */
export const deleteAllTranslations = async (user) => {
    const content = {
        username: user.username,
        translations: [],
    }
    try {
        const response = await axios.patch(`${API_URL}/${user.id}`, content, { headers })
        if (response.status !== 200) {
            throw new Error("Could not remove translations");

        }
        return [null, response.data];
    } catch (error) {
        return [error.message, null];
    }
}
