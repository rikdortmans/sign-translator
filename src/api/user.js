import axios from "axios"

const API_URL = process.env.REACT_APP_API_URL
const API_KEY = process.env.REACT_APP_API_KEY

/**
 * Gets the user from the API based on which username is given
 * Used by loginUser()
 * @param {string} username of the user
 * @returns {[Error, response.data]}   
 */
const getUser = async (username) => {

    console.log(username);
    try {
        if (username == undefined) {
            throw new Error('Username undefined')
        }
        const response = await axios.get(`${API_URL}?username=${username}`);
        if (response.status !== 200) {
            throw new Error('Could not complete request');
        }
        return [null, response.data]
    } catch (error) {
        return [error.message, []];
    }
}

/**
 * Creates a new User on the API with empty translations
 * Used by getUser() if no user is found
 * @param {string} username of the user that needs to be created
 * @returns {[Error, response.data]}   
 */
const createUser = async (username) => {
    const headers = {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY,
    }

    const newUser = {
        username,
        translations: [],
    }

    try {
        const response = await axios.post(API_URL, newUser, { headers });

        if (response.status !== 201) {
            throw new Error('Could not create user: ' + username);
        }
        return [null, response.data]
    } catch (error) {
        return [error.message, []];
    }
}

/**
 * First gets the user logging in from the API and returns the response
 * If no user is found, a new one is created and returned
 * Used by components/Login/LoginForm.jsx
 * @param {string} username of the user logging in
  * @returns {[Error, response.data]}
 */
export const loginUser = async (username) => {
    const [checkError, user] = await getUser(username);

    if (checkError !== null) {
        return [checkError, null];
    }

    if (user.length > 0) {
        return [null, user.pop()]
    }

    return await createUser(username);
}