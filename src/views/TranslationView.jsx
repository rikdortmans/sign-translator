import TranslatorForm from "../components/Translator/TranslatorForm";
import withAuth from "../hoc/withAuth";

const TranslationView = () => {
    return (
        <>
            <TranslatorForm>
            </TranslatorForm>
        </>
    )
}

export default withAuth(TranslationView);