import { useState } from "react";
import { Button } from "react-bootstrap";
import { deleteAllTranslations, getAllTranslations } from "../api/translation";
import ClearHistoryButton from "../components/Profile/ClearHistoryButton";
import TranslationHistory from "../components/Profile/TranslationHistory";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage";

const Profile = () => {

    return (
        <>
            <section>
                <TranslationHistory />
                
            </section>
        </>
    )
}

export default withAuth(Profile);