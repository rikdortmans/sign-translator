import './App.css';
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Login from './views/Login';
import TranslationView from './views/TranslationView';
import Profile from './views/Profile';
import Navbar from './components/Navbar/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
      <BrowserRouter>
      <div className="App">
        <Navbar></Navbar>
        <div className='container-md'>
        <Routes>
          <Route path="/" element={<Login/>} />
          <Route path="/translator" element={<TranslationView/>}/>
          <Route path="/profile" element={<Profile/>}/>
        </Routes>

        </div>
      </div>
    </BrowserRouter>

  );
}

export default App;
